derby = require 'derby'

app = module.exports = derby.createApp 'game', __filename


app.use require 'd-bootstrap'
app.use require 'derby-router'
app.use require 'derby-debug'
app.serverUse module, 'derby-jade'
app.serverUse module, 'derby-stylus'

app.loadViews __dirname + '/views'
app.loadStyles __dirname + '/styles'

app.component require '../../components/gameList/gameList'
app.component require '../../components/gameplay/gameplay'

PLAYERS_COUNT = 1;

app.get '/', (page)->
  page.render 'home'


app.get '/game/:id', (page, model, params, next) ->
  page.render 'game'

app.get '/games', (page, model, params, next) ->
  page.render 'games'


app.proto.startGame = ->
  model = this.model
  userId = model.get '_session.userId'
  model.set '_page.game.players.' + userId, true
  game = model.get '_page.game'
  if Object.keys(game.players).length == PLAYERS_COUNT
    model.set '_page.game.level', 0
    model.set '_page.game.started', true


app.proto.addAnswer = ->
  console.log "addAnswer"
  model = this.model
  userId = model.get '_session.userId'
  answer = model.get '_page.answer'
  level = model.get '_page.game.level'
  model.set '_page.game.usersCount', PLAYERS_COUNT
  model.set '_page.game.answers.' + level + "." + userId, answer
  console.log model.get '_page.game.answers.' + level
  answerObj = model.get '_page.game.answers.' + level
  if Object.keys(answerObj).length == PLAYERS_COUNT
    p = 45 - 0.2 * Object.keys(answerObj).map((key) ->
      answerObj[key]
    ).reduce (prev, next) ->
      prev + next
    console.log "", p
    model.set '_page.game.p', p
    model.set '_page.game.level', level + 1


app.proto.saveUserName = ->
  userId = this.model.get '_session.userId'
  userName = this.model.get '_page.userName'
  console.log this.model.get 'users.' + userId
  if userName?
    userName = userName.trim()
  if userName
    this.model.add 'users.' + userId, {username: userName}
  else
    alert 'Name cannot be empty!'

  console.log this.model.get 'users'

