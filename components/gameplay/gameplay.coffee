gameplay = ->
module.exports = gameplay

gameplay::view = __dirname + "/gameplay"

gameplay::init = (model)->


gameplay::create = (model)->
  model.root.subscribe 'game', ->
    model.root.at('games.' + this.page.params.id).ref '_page.game'

gameplay::connectToGame = ->
  userId = this.model.root.get '_session.userId'
  @game.set 'players.' + userId, true
  if Object.keys(@game.get 'players').length == 1
    this.model.root.set '_page.players.ready', true
  console.log '#root._page.game.players'


gameplay::startGame = ->
  console.log "start"
  @game.set 'started', true
  @game.set 'currentLevel', 0

gameplay::addAnswer = ->
  this.model.root.set "test.val" , 1
  userId = this.model.root.get '_session.userId'
  answer = this.model.root.get '_page.answer'
  level = @game.get 'currentLevel'
  @game.set 'levels.' + level + ".answers." + userId, answer
  levelAnswers = @game.get 'levels.' + level + '.answers'

  if Object.keys(levelAnswers).length == 1
    cost = 5
    profit = {}
    summ = Object.keys(levelAnswers).map((key) ->
      levelAnswers[key]
    ).reduce (prev, next) ->
      prev + next
    price = 45 - 0.2 * summ


    @game.set 'levels.' + level + '.price', price

    for prop of levelAnswers
      console.log prop
      @game.set 'levels.' + level + '.profit', levelAnswers[prop] * ( price - cost)

    @game.set 'currentLevel', level + 1
    console.log @game.get 'levels'