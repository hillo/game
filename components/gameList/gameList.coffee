gameList = ->

module.exports = gameList

gameList::view = __dirname + "/gameList"

gameList::init = (model) ->
  model.root.subscribe 'games', ->
    model.root.at('games').filter().ref '_page.games'

gameList::create = (model)  ->
  model.root.subscribe 'games', ->
    model.root.at('games').filter().ref '_page.games'

gameList::addGame = ->
  gameName = this.model.root.get "_page.newGameName"
  this.model.root.add 'games', {
    name: gameName,
    createdBy: this.model.root.get '_session.userId'
  }

gameList::removeGame = (id)->
  this.model.root.del 'games.' + id